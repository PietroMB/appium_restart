FROM python:3.11

# COPY . .
WORKDIR /usr/src/app
COPY requirements.txt .
COPY code/* ./

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install python dependencies
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

# run uvicorn
CMD ["uvicorn", "main:app", "--host", "127.0.0.0", "--reload"]
