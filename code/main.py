import json, os, time, datetime, logging
from fastapi import FastAPI
from paramiko import SSHClient, AutoAddPolicy

# logs
if not os.path.exists("./log"):
  os.mkdir("./log")
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    level=logging.INFO,
    filename='log/call.log'
)
logger = logging.getLogger('call')


# fastapi init
app = FastAPI()


@app.get("/")
def test():
    """
        Perform test and check it's all ok,
        this test will also provide server timestamp
    """
    return {
        "info": "this app uses UTC time",
        "unix": datetime.datetime.utcnow().timestamp(), 
        "datetime": datetime.datetime.utcnow()
    }



@app.get("/riavvia_adb/")
def reboot_adb():
    """
        Perform docker appium restart via SSH connection
    """
    # vars
    host = os.environ.get('SSH_HOST') or '127.0.0.0'
    username = os.environ.get('SSH_USER') or 'user'
    password = os.environ.get('SSH_PASS') or 'secret'
    port = int(os.environ.get('SSH_PORT') or '22')
    cmd_to_execute = "docker rm -f appium-container; docker run --privileged -d -p 4723:4723  -v /dev/bus/usb:/dev/bus/usb --name appium-container appium/appium"

    # exec
    logger.info("reboot appium function started")
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    ssh.connect(host, username=username, password=password, port=port)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd_to_execute)
    
    if ssh_stderr:
        logger.error(str(ssh_stderr))
    
    return {"response": "ok"}
    